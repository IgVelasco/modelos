import numpy as np
from numpy import genfromtxt

data = genfromtxt('dataset_entrega.txt', delimiter=',').astype(int)
np.delete(data, 0)
data = data.tolist()


# print(data)

# - i = 1
# - Asignar el dia i a una familia
# - Mientras que queden familias sin un dia asignado, asignar el dia i a todas las familias que se puedan  // MAXIMA CANTIDAD DE FAMILIAS A ASIGNAR TIENE QUE SER N/DIAS
# - Consideraciones: 2 familias pueden salir el mismo dia si cumplen las # siguientes condiciones:
#    1. No son vecinas
#    2. No se puede asignar ese dia si superan las x cantidad de familias por dia, x es una variable conocida, por       ejemplo vamos a tomar x = TotalDeFamilias/Dias
# - Si no se les puede asignar el dia i a mas familias, incrementar i y volver a intentar con el resto de las familias


def coloreo(data):
    dias_de_salida = [None] * len(data)
    dia_actual = 1
    familias_sin_dia = []
    salen_ese_dia = []

    max_dias = 10


    for x in range(0, len(data)):
        familias_sin_dia.append(x)

    while (len(familias_sin_dia) > 0):
        salen_ese_dia = []
        for i in range(0, len(familias_sin_dia)):
            familia_actual = familias_sin_dia[i]
            se_puede = True
            for k in range(0, len(salen_ese_dia)):
                data_dia = data[familias_sin_dia[i]][salen_ese_dia[k]]
                if (data[familias_sin_dia[i]][salen_ese_dia[k]] == 1):
                    se_puede = False
                    break
            if (se_puede):
                dias_de_salida[familia_actual] = dia_actual
                salen_ese_dia.append(familia_actual)
                continue

        for familia in salen_ese_dia:
            if familia in familias_sin_dia:
                familias_sin_dia.remove(familia)

        print("Salen ese dia " + str(dia_actual) + ": " + str(salen_ese_dia))
        dia_actual += 1
    return dias_de_salida


dias = coloreo(data)
print("Result: " + str(dias))

print(data[13][20])
# Dia 1 = 0, 2, 4, 9
# Dia 2 = 1, 3, 6
# Dia 3 = 5,7
# Dia 4 = 8